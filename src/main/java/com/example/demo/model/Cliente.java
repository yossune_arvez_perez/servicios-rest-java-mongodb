package com.example.demo.model;


import java.util.Date;

import javax.annotation.Generated;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Cliente")
public class Cliente {
	
	@Transient
	public static final String SECUENCE_NAME="user_secuence";
	@Id
	private long Cliente_ID;
	private String Nombre_Usuario;
	private String Contrasenia;
	private String Nombre;
	private String Apellidos;
	private String Correo_Electronico;
	private int Edad;
	private double Estatura;
	private double Peso;
	private double IMC;
	private double GEB;
	private double ETA;
	private Date Fecha_Creacion;
	private Date Fecha_Actualizacion;
	
	public Cliente() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Cliente(String Nombre_Usuario, String Contrasenia, String Nombre, String Apellidos,
			String correo_Electronico, int Edad, double Estatura, double Peso, double IMC, double GEB, double ETA,
			Date Fecha_Creacion, Date Fecha_Actualizacion,long cliente_ID) {
		super();
	
		this.Nombre_Usuario = Nombre_Usuario;
		this.Contrasenia = Contrasenia;
		this.Nombre = Nombre;
		this.Apellidos = Apellidos;
		this.Correo_Electronico = correo_Electronico;
		this.Edad = Edad;
		this.Estatura = Estatura;
		this.Peso = Peso;
		this.IMC = IMC;
		this.GEB = GEB;
		this.ETA = ETA;
		this.Fecha_Creacion = Fecha_Creacion;
		this.Fecha_Actualizacion = Fecha_Actualizacion;
		this.Cliente_ID = cliente_ID;
	}

	

	public long getCliente_ID() {
		return Cliente_ID;
	}

	public void setCliente_ID(long cliente_ID) {
		Cliente_ID = cliente_ID;
	}

	public String getNombre_Usuario() {
		return Nombre_Usuario;
	}

	public void setNombre_Usuario(String nombre_Usuario) {
		Nombre_Usuario = nombre_Usuario;
	}

	public String getContrasenia() {
		return Contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		Contrasenia = contrasenia;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public String getApellidos() {
		return Apellidos;
	}

	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}

	public String getCorreo_Electronico() {
		return Correo_Electronico;
	}

	public void setCorreo_Electronico(String correo_Electronico) {
		this.Correo_Electronico = correo_Electronico;
	}

	public int getEdad() {
		return Edad;
	}

	public void setEdad(int edad) {
		Edad = edad;
	}

	public double getEstatura() {
		return Estatura;
	}

	public void setEstatura(double estatura) {
		Estatura = estatura;
	}

	public double getPeso() {
		return Peso;
	}

	public void setPeso(double peso) {
		Peso = peso;
	}

	public double getIMC() {
		return IMC;
	}

	public void setIMC(double iMC) {
		IMC = iMC;
	}

	public double getGEB() {
		return GEB;
	}

	public void setGEB(double gEB) {
		GEB = gEB;
	}

	public double getETA() {
		return ETA;
	}

	public void setETA(double eTA) {
		ETA = eTA;
	}

	public Date getFecha_Creacion() {
		return Fecha_Creacion;
	}

	public void setFecha_Creacion(Date fecha_Creacion) {
		Fecha_Creacion = fecha_Creacion;
	}

	public Date getFecha_Actualizacion() {
		return Fecha_Actualizacion;
	}

	public void setFecha_Actualizacion(Date fecha_Actualizacion) {
		Fecha_Actualizacion = fecha_Actualizacion;
	}


}
