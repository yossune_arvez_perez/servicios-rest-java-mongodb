package com.example.demo.response;

public class Message {
	
	public static String ErrorUsuario = "Usuario existente";
	public static int errorusuario = -1;
	public static String Exitoso = "Registro Exitoso";
	public static String PutExitoso = "Datos Actualizado Correctamente";
	public static String PutError = "Error al actualizar";
	public static int puterror = -2;
	public static String PutErrorID = "Id no agregado, Ejemplo: PUT/NutriNET/Cliente/5";
	public static int puterrorID = -3;
	public static int Correcto = 0;
	
	private int Cve_Error;
	private String Cve_Mensaje ;
	public Message(int cve_Error, String cve_Mensaje) {
		super();
		Cve_Error = cve_Error;
		Cve_Mensaje = cve_Mensaje;
	}
	public Message() {
	
		// TODO Auto-generated constructor stub
	}
	public int getCve_Error() {
		return Cve_Error;
	}
	public void setCve_Error(int cve_Error) {
		Cve_Error = cve_Error;
	}
	public String getCve_Mensaje() {
		return Cve_Mensaje;
	}
	public void setCve_Mensaje(String cve_Mensaje) {
		Cve_Mensaje = cve_Mensaje;
	}
	
	
	
	
	
	
	

	
}
