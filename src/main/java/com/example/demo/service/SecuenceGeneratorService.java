package com.example.demo.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import com.example.demo.model.DbSecuence;

import org.springframework.data.mongodb.core.FindAndModifyOptions;



@Service
public class SecuenceGeneratorService {
	
	@Autowired
	private MongoOperations mongoOperations;
	
public int getSecuenNumber(String secName) {
	Query query = new Query (Criteria.where("_id").is(secName));
	Update update = new Update().inc("seq",1);
	FindAndModifyOptions options = new FindAndModifyOptions();
    options.upsert(true);
    options.returnNew(true);
	DbSecuence counter = mongoOperations
			.findAndModify(query,
					update, options,
					DbSecuence.class);
	
	return !Objects.isNull(counter)? counter.getSeq():1;
}
}
