package com.example.demo.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Cliente;
import com.example.demo.repositories.ClienteRepository;
import com.example.demo.response.Message;
import com.example.demo.service.SecuenceGeneratorService;
import com.google.gson.Gson;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.internal.async.SingleResultCallback;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.currentDate;
import static com.mongodb.client.model.Updates.set;


@RestController
public class ClienteController {
	
	@Autowired
	public ClienteRepository clienterepository;
	
	@Autowired
	private SecuenceGeneratorService service ;
	
	@Autowired
	private MongoOperations mongoOperations;
	
	public ClienteController() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	// metodo para crear Cliente
	
	@PostMapping(value="POST/NutriNET/Cliente",consumes="application/json")
	public Message CreateCliente(@RequestBody Cliente cliente) {
		
		Cliente client = null;
		List<Cliente> clientes = clienterepository.findAll();
		//String Mensaje = "correcto";
		Message mensaje = new Message();
		boolean bandera = true;
		
		if (clientes.size() >0 ) {
			for (int i = 0; i < clientes.size(); i++) {
				
				
				
				if (clientes.get(i).getCorreo_Electronico().equals(cliente.getCorreo_Electronico())) {					
					mensaje.setCve_Error(Message.errorusuario);
					mensaje.setCve_Mensaje(Message.ErrorUsuario);	
					bandera = false;

					break;
				}
				
			
			else if (clientes.get(i).getNombre_Usuario().equals(cliente.getNombre_Usuario())){
				mensaje.setCve_Error(Message.errorusuario);
				mensaje.setCve_Mensaje(Message.ErrorUsuario);	
				bandera = false;
				break;
				
			}
			else {
				bandera = true;
			}
			}
			
		}
		if (bandera)  {
			cliente.setCliente_ID(service.getSecuenNumber(Cliente.SECUENCE_NAME));
			cliente.setFecha_Creacion(new Date());
			 client= clienterepository.insert(cliente);
			 mensaje.setCve_Error(Message.Correcto);
			 mensaje.setCve_Mensaje(Message.Exitoso);	
			
		}
		
		return mensaje;
	}
	
	
	// metodo para ontener un cliente por ID o la lista completa en caso de id null

	@GetMapping(value="GET/NutriNET/Cliente")
	public List<Cliente> getClienteId (@RequestParam( required=false) Long cliente_ID) {
		
		List<Cliente> clientes ;
		//Long id = (long) 2;
		
		if ( cliente_ID == null) {
			clientes = clienterepository.findAll();
		}
		
		else {
						
			MongoCollection<Document> collection = mongoOperations.getCollection("Cliente");
		
			Document documento = collection.find(eq("_id", cliente_ID)).sort(new Document("_id", -1)).first();	
			if (documento == null) {
				 clientes = new ArrayList<Cliente>();
			}else {
				 Gson gson = new Gson();
			 JSONObject json = new JSONObject(documento.toJson());  
			 json.remove("Fecha_Actualizacion");	
			 json.remove("Fecha_Creacion");	
			 Cliente cliente = gson.fromJson(json.toString(), Cliente.class);	
			 cliente.setCliente_ID(cliente_ID);
			 clientes = new ArrayList<Cliente>();
			 clientes.add(cliente);
			}
			
		}
		
		
		
		return clientes;
		
		
	}

	// metodo para actualizar un cliente, en caso de no agregar el ID este retorna un mensaje de error y un ejemplo de la liga 
	@PutMapping(value={"PUT/NutriNET/Cliente/" , "PUT/NutriNET/Cliente/{Cliente_ID}"})
	public Message updateCliente( @PathVariable( required=false)  Long  Cliente_ID ,@RequestBody Cliente cliente) {
		Message mensaje = new Message();
		
		if (Cliente_ID == null) {
			
			mensaje.setCve_Error(Message.puterrorID);
			 mensaje.setCve_Mensaje(Message.PutErrorID);
		}else
		{
			MongoCollection<Document> collection = mongoOperations.getCollection("Cliente");
			
			collection.updateOne(eq("_id", Cliente_ID),
	                combine(set("Edad", cliente.getEdad()),
	                        set("Estatura",cliente.getEstatura()),
	                        set("Peso",cliente.getPeso()),
	                        set("GEB",cliente.getGEB()),                       
	                        currentDate("Fecha_Actualizacion")
	                        )
	               );
		
			 mensaje.setCve_Error(Message.Correcto);
			 mensaje.setCve_Mensaje(Message.PutExitoso);
			
		}
		
		
		
		return mensaje;
	}
	
	

}
